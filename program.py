import os
import datetime
from shutil import copyfile

###
###где забирать файлы
work_dir = "/home/joinack/git/copyfreshfile/"
###куда ложить файлы
backup_dir = "/home/joinack/git/copyfreshfile/new_folder/"
###что считать старыми файлами в днях
old_files_time = 50
###

#создаём список файлов для перемещения в новый каталог
list_to_copy = []

#получаем список файлов в папке
file_dir = os.path.dirname(os.path.relpath(__file__))
files = [
    f for f in os.listdir(work_dir)
    if f.endswith(".xz")]
print(f"Файлы в папке: {files}")

#получаем путь, дату создания и сравниваем с текущей датой, если меньше old_files_time дней, тогда добовляем в список для копирования
def get_info_file(list_files):
    for i in list_files:
      # узнаём когда создан файл как toordinal - количество дней, прошедших с 01.01.1970.
      create_date = datetime.datetime.toordinal(datetime.datetime.fromtimestamp(os.stat(i).st_mtime))
      #узнаём текущую дату как toordinal
      current_datetime = datetime.datetime.toordinal(datetime.date.today())
      #узнаём разницу времени
      diff_time = current_datetime - create_date
      #добовляем файл в список для копирования
      if diff_time < old_files_time:
          list_to_copy.append(i)
      
  
get_info_file(files)
print(f"Эти файлы будут скопированы: {list_to_copy}")

#for i in list_to_copy:
#    pr = i - workdir
#    print(pr)

def copy_file(list_to_copy):
    for i in list_to_copy:
      from_backup_path = work_dir + i
      to_backup_path = backup_dir + i
      copyfile(from_backup_path, to_backup_path)

copy_file(list_to_copy)
